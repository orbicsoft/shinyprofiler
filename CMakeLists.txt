cmake_minimum_required(VERSION 3.6)

project(Shiny LANGUAGES C CXX)

set(SHINY_BASE_FOLDER vendor/Shiny)

# SHINY_HOSTED_BUILD should be set to TRUE when Shiny is included
# in another project's CMake build

if (SHINY_HOSTED_BUILD)
    set(_SHINY_DEFAULT_BUILD_SAMPLES OFF)
else()
    set(_SHINY_DEFAULT_BUILD_SAMPLES ON)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
endif()

set(SHINY_BUILD_SAMPLES ${_SHINY_DEFAULT_BUILD_SAMPLES} CACHE BOOL "Build Shiny samples")
set(SHINY_VERBOSE_ANDROID_LOGGING FALSE CACHE BOOL "Enable verbose Android logging")
unset(_SHINY_DEFAULT_BUILD_SAMPLES)

set(SHINY_HEADERS
    include/Shiny.h
    include/ShinyConfig.h
    include/ShinyData.h
    include/ShinyMacros.h
    include/ShinyManager.h
    include/ShinyNode.h
    include/ShinyNodePool.h
    include/ShinyNodeState.h
    include/ShinyOutput.h
    include/ShinyPrereqs.h
    include/ShinyTools.h
    include/ShinyVersion.h
    include/ShinyZone.h)

set(SHINY_SOURCES
    src/ShinyManager.cpp
    src/ShinyNode.c
    src/ShinyNodePool.c
    src/ShinyNodeState.c
    src/ShinyOutput.cpp
    src/ShinyTools.cpp
    src/ShinyZone.c)

if (NOT WIN32)
  add_definitions(-fvisibility=hidden)
endif()

if (ANDROID AND SHINY_VERBOSE_ANDROID_LOGGING)
  add_definitions(-DSHINY_VERBOSE_ANDROID_LOGGING=1)
endif()

add_library(Shiny STATIC ${SHINY_SOURCES} ${SHINY_HEADERS})

if (ANDROID)
  target_link_libraries(Shiny PUBLIC log)
endif()

# define variable for legacy usages
set(SHINY_INCLUDE_DIR ${CMAKE_CURRENT_LIST_DIR}/include)
target_include_directories(Shiny PUBLIC ${SHINY_INCLUDE_DIR})

if (MSVC)
    set(COMPILE_OPTIONS /W4)
else()
    set(COMPILE_OPTIONS -Wall)
endif()

target_compile_options(Shiny PRIVATE ${COMPILE_OPTIONS})

set(SHINY_TARGET_PROPERTIES
  FOLDER ${SHINY_BASE_FOLDER}
  POSITION_INDEPENDENT_CODE YES
  CXX_STANDARD 11
  CXX_STANDARD_REQUIRED YES)

get_target_property(SHINY_LIBRARY_TYPE Shiny TYPE)
if (${SHINY_LIBRARY_TYPE} STREQUAL STATIC_LIBRARY)
    set(LIBRARY_SUFFIX -static)
endif()

set_target_properties(
    Shiny
    PROPERTIES
    ${SHINY_TARGET_PROPERTIES}
    OUTPUT_NAME Shiny${LIBRARY_SUFFIX})

if (SHINY_BUILD_SAMPLES)
    if (SHINY_HOSTED_BUILD)
        message(WARNING "Shiny Samples: Compilation errors may occur when Shiny is hosted")
    endif()

    add_subdirectory(Samples)
    add_custom_target(ShinySamples ALL)

    set_target_properties(
        ShinySamples
        PROPERTIES
        FOLDER ${SHINY_BASE_FOLDER})

    add_dependencies(ShinySamples
        ShinySimpleSample
        ShinyMainLoopSample
        ShinyAdvancedSample
        ShinyThreadSample)
endif()
